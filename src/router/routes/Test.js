// ** React Imports
import {lazy} from 'react'
// import { Navigate } from 'react-router-dom'

const Modal = lazy(() => import('@services/test/pages/modal'))

const TestRoutes = [
  {
    element: <Modal />,
    path: '/test/modal',
  },
]

export default TestRoutes
