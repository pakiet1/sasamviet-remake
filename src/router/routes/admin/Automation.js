// ** React Imports
import { lazy } from "react";

const AutoJob = lazy(() => import("@services/automation/pages/AutomationPage"));

const AutomationRoutes = [
  {
    element: <AutoJob />,
    path: "/admin/automation/auto-job",
  },
];

export default AutomationRoutes;
