// ** React Imports
import { lazy } from "react";

const Dashboard = lazy(() => import("@services/automation/pages/dashboard/"));

const AutomationRoutes = [
  {
    element: <Dashboard />,
    exact: true,
    path: "/admin/automation/dashboard/account-facebook",
  },
];

export default AutomationRoutes;