import {
  Database,
  Circle,
  User,
  Aperture,
  ShoppingCart,
  Mail,
} from 'react-feather'

const admin = [
  {
    header: 'Admin Management',
  },
  {
    id: 'customer',
    title: 'Khách hàng',
    icon: <User size={20} />,
    children: [
      {
        id: 'customer-normal',
        title: 'Khách hàng',
        icon: <Circle size={12} />,
        navLink: '/admin/customer',
      },
      {
        id: 'customer-lead',
        title: 'Khách hàng tiềm năng',
        icon: <Circle size={12} />,
        navLink: '/admin/potential-customers',
      },
      // {
      //   id: 'fb-care',
      //   title: 'Chăm sóc Facebook',
      //   icon: <Circle size={12} />,
      //   navLink: '/admin/take-care-of-facebook',
      // },
      {
        id: 'exploit-customer-lead',
        title: 'Khai thác KHTN',
        icon: <Circle size={12} />,
        navLink: '/admin/exploit-potential-customers',
      },
    ],
  },
  {
    id: 'marketing',
    title: 'Marketing',
    icon: <Aperture size={20} />,
    children: [
      {
        id: 'dashboard',
        title: 'Dashboard',
        icon: <Circle size={12} />,
        navLink: '/admin/automation/dashboard/account-facebook',
      },
      {
        id: 'automation',
        title: 'Automation',
        icon: <Circle size={12} />,
        navLink: '/admin/automation/auto-job',
      },
      {
        id: 'campain',
        title: 'Chiến dịch',
        icon: <Circle size={12} />,
        navLink: '/admin/campaign',
      },
      // {
      //   id: 'email-admin',
      //   title: 'Email',
      //   icon: <Circle size={12} />,
      //   navLink: '/admin/email',
      // },
      // {
      //   id: 'call-cente',
      //   title: 'Cuộc gọi',
      //   icon: <Circle size={12} />,
      //   navLink: '/admin/call-cateloget',
      // },
      // {
      //   id: 'social-network',
      //   title: 'Mạng xã hội',
      //   icon: <Circle size={12} />,
      //   navLink: '/admin/social-network',
      // },
      {
        id: 'accout-facebook',
        title: 'Tài khoản Facebook',
        icon: <Circle size={12} />,
        navLink: '/admin/automation/account-facebook/list',
      },
      {
        id: 'list-post',
        title: 'Danh sách bài viết',
        icon: <Circle size={12} />,
        navLink: '/admin/automation/posts',
      },
    ],
  },
  // {
  //   id: 'admin',
  //   title: 'Quản trị khác',
  //   icon: <Database size={20} />,
  //   children: [
  //     {
  //       id: 'management-admin',
  //       title: 'Admin',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/admin',
  //     },

  //     {
  //       id: 'admin-member',
  //       title: 'Thành viên',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/members',
  //     },
  //   ],
  // },
  // {
  //   id: 'admin-cms',
  //   title: 'CMS',
  //   icon: <Mail size={20} />,
  //   children: [
  //     {
  //       id: 'seo',
  //       title: 'SEO',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/cms/seo',
  //     },
  //     // {
  //     //   id: 'sasamviet',
  //     //   title: 'Sasam Việt',
  //     //   icon: <Circle size={12} />,
  //     //   navLink: '/admin/cms/sasamviet',
  //     // },
  //     // {
  //     //   id: 'member-company',
  //     //   title: 'Cty Thành viên',
  //     //   icon: <Circle size={12} />,
  //     //   navLink: '/admin/cms/cty-thanh-vien',
  //     // },
  //     {
  //       id: 'achievement',
  //       title: 'Thành tựu',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/cms/achievement/list',
  //     },
  //     // {
  //     //   id: 'press-media',
  //     //   title: 'Báo chí & Truyền thông',
  //     //   icon: <Circle size={12} />,
  //     //   navLink: '/admin/cms/bao-chi-truyen-thong',
  //     // },
  //     {
  //       id: 'social-activites',
  //       title: 'Hoạt động xã hội',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/cms/community_activity/list',
  //     },
  //     {
  //       id: 'news',
  //       title: 'Tin tức',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/cms/news/list',
  //     },
  //     // {
  //     //   id: 'partner',
  //     //   title: 'Đối tác',
  //     //   icon: <Circle size={12} />,
  //     //   navLink: '/admin/cms/doi-tac',
  //     // },
  //     {
  //       id: 'contact',
  //       title: 'Liên hệ',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/cms/lien-he',
  //     },
  //   ],
  // },
  // {
  //   id: 'ecommerce',
  //   title: 'Thương mại điện tử',
  //   icon: <ShoppingCart size={20} />,
  //   navLink: '/admin/thuong-mai-dien-tu',
  //   children: [
  //     {
  //       id: 'product',
  //       title: 'Sản phẩm',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/san-pham',
  //     },
  //     {
  //       id: 'landing_page',
  //       title: 'Landing page',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/landing-page',
  //     },
  //     {
  //       id: 'cart',
  //       title: 'Giỏ hàng',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/gio-hang',
  //     },
  //     {
  //       id: 'order',
  //       title: 'Đơn hàng',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/don-hang',
  //     },
  //     {
  //       id: 'shipping',
  //       title: 'Hình thức giao hàng',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/shipping',
  //     },
  //     {
  //       id: 'shipment',
  //       title: 'Vận chuyển',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/shipment',
  //     },
  //     // {
  //     //   id: 'category',
  //     //   title: 'Danh mục sản phẩm',
  //     //   icon: <Circle size={12} />,
  //     //   navLink: '/admin/thuong-mai-dien-tu/danh-muc-san-pham',
  //     // },
  //     {
  //       id: 'type-model',
  //       title: 'Danh mục sản phẩm',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/loai-va-mat-hang',
  //     },
  //     {
  //       id: 'tag',
  //       title: 'Tag',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/tag',
  //     },
  //     {
  //       id: 'Attribute',
  //       title: 'Thuộc tính sản phẩm',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/thuoc-tinh',
  //     },
  //     {
  //       id: 'label',
  //       title: 'Nhãn',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/label',
  //     },
  //     {
  //       id: 'collection',
  //       title: 'Bộ sưu tập',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/bo-suu-tap',
  //     },
  //     {
  //       id: 'review',
  //       title: 'Đánh giá',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/review',
  //     },
  //     {
  //       id: 'banner',
  //       title: 'Banner',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/banner',
  //     },
  //     {
  //       id: 'discount',
  //       title: 'Khuyến mãi',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/khuyen-mai',
  //     },
  //     {
  //       id: 'flash-cross-Sale',
  //       title: 'Flash / cross Sale',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/flash-cross-Sale',
  //     },

  //     {
  //       id: 'tax',
  //       title: 'Thuế',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/tax',
  //     },
  //     // {
  //     //   id: 'report',
  //     //   title: 'Thống kê',
  //     //   icon: <Circle size={12} />,
  //     //   navLink: '/admin/thuong-mai-dien-tu/thong-ke',
  //     // },
  //     // {
  //     //   id: 'seo',
  //     //   title: 'SEO engine',
  //     //   icon: <Circle size={12} />,
  //     //   navLink: '/admin/thuong-mai-dien-tu/seo-engine',
  //     // },
  //     {
  //       id: 'transaction',
  //       title: 'Lịch sử giao dịch',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/lich-su-giao-dich',
  //     },
  //     {
  //       id: 'payment-method',
  //       title: 'Phương thức thanh toán',
  //       icon: <Circle size={12} />,
  //       navLink: '/admin/thuong-mai-dien-tu/phuong-thuc-thanh-toan',
  //     },
  //   ],
  // },
]

export default admin
