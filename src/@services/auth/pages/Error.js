// ** React Imports
import {Link} from 'react-router-dom'

// ** Reactstrap Imports
import {Button} from 'reactstrap'

// ** Custom Hooks
import {useSkin} from 'utility/hooks/useSkin'

// *** URL
import {LOGO_URL} from '@services/auth/constants/url'

// ** Styles
import '@core/scss/base/pages/page-misc.scss'

const ErrorPage = () => {
  // ** Hooks
  const {skin} = useSkin()

  const illustration = skin === 'dark' ? 'error-dark.svg' : 'error.svg',
    source = require(`assets/images/pages/${illustration}`).default
  return (
    <div className="misc-wrapper">
      <a className="brand-logo" href="/">
        <img src={LOGO_URL} alt="brand-logo" height={150} />
      </a>
      <div className="misc-inner p-2 p-sm-3">
        <div className="w-100 text-center">
          <h2 className="mb-1">Page Not Found 🕵🏻‍♀️</h2>
          <p className="mb-2">
            Oops! 😖 The requested URL was not found on this server.
          </p>
          <Button
            tag={Link}
            to="/"
            color="primary"
            className="btn-sm-block mb-2"
          >
            Back to home
          </Button>
          <img className="img-fluid" src={source} alt="Not authorized page" />
        </div>
      </div>
    </div>
  )
}
export default ErrorPage
