export const queryKeys = {
  auth: {
    register: 'REGISTER',
    login: 'LOGIN',
    verify_account: 'VERIFY_ACCOUNT',
    reset_password: 'RESET_PASSWORD',
    send_mail_reset_password: 'SEND_MAIL_RESET_PASSWORD',
    change_password: 'CHANGE_PASSWORD',
    update_password: 'UPDATE_PASSWORD',
  },
  customer: {
    // *** Chăm sóc facebook
    personalFacebookStatistic: 'PERSONAL_FACEBOOK_STATISTIC',

    // *** Khai thác khách hàng tiềm năng
    listTagsCustomerLead: 'LIST_TAGS_CUSTOMER_LEAD',
    listEducationOfCustomerLead: 'LIST_EDUCATION_OF_CUSTOMER_LEAD',
    listJobOfCustomerLead: 'LIST_JOB_OF_CUSTOMER_LEAD',
    listPlacesLivedCustomerLead: 'LIST_PLACES_LIVED_CUSTOMER_LEAD',

    // *** Khách hàng tiềm năng
    listFunnelActionCustomerLead: 'LIST_FUNNEL_ACTION_CUSTOMER_LEAD',
    listFunnelOperatorCustomerLead: 'LIST_FUNNEL_OPERATOR_CUSTOMER_LEAD',
    listFunnelCustomerLead: 'LIST_FUNNEL_CUSTOMER_LEAD',

    // *** User
    linkMyPostStatistic: 'LINK_MY_POST_STATISTIC',
    listMyPostLink: 'LIST_MY_POST_LINK',

    // *** Admin
    // *** Group
    groupExploitedStatistic: 'GROUP_EXPLOITED_STATISTIS',
    recentlyExtractedLink: 'RECENTLY_EXTRACTED_LINK',
    detailGroupExploiteds: 'DETAIL_GROUP_EXPLOITEDS',
    listGroupExploiteds: 'LIST_GROUP_EXPLOITEDS',

    // *** Post
    postExploitedStatistic: 'POST_EXPLOITED_STATISTIC',
    listPostExploiteds: 'LIST_POST_EXPLOITEDS',
    detailPostExploited: 'DETAIL_POST_EXPLOITEDS',

    // *** Fanpage
    fanpageExploitedStatistic: 'FANPAGE_EXPLOITED_STATISTIC',
    fanpageExploitedInfo: 'FANPAGE_EXPLOITED_INFO',

    // *** Profile
    profileExploitedStatistic: 'PROFILE_EXPLOITED_STATISTIC',
    profileExploitedInfo: 'PROFILE_EXPLOITED_INFO',
    listProfileExploiteds: 'LIST_CUSTOMER_LEAD_PROFILE_EXPLOITEDS',
    listProfileExploitedExports: 'LIST_PROFILE_EXPLOITED_EXPORTS',
    listCustomerKeywords: 'LIST_CUSTOMER_KEYWORDS',
  },
  automation: {
    status: 'ALL_STATUS_AUTOMATION',
    createAutoJob: 'CREATE_AUTO_JOB',
    getStatisticAutomation: 'GET_STATISTIC_AUTOMATION',
    getBlockTypeAction: 'GET_BLOCK_TYPE_ACTION',
    getBlockTypeDecision: 'GET_BLOCK_TYPE_DECISION',
    getBlockTypeFBAction: 'GET_BLOCK_TYPE_FB_ACTION',
    getListAutoJob: 'GET_LIST_AUTO_JOB',
    getListAutoTask: 'GET_LIST_AUTO_TASK',
    info: 'GET_AUTOMATION_INFOMATION',
    getListAccountFB: 'GET_LIST_ACCOUNT_FB',
    getListEmailToSendEmail: 'GET_LIST_EMAIL_TO_SEND_EMAIL',
    getListLibraryNotification: 'GET_LIST_LIBRARY_NOTIFICATION',
    getListStatusAutomation: 'GET_LIST_STATUS_AUTOMATION',
    getListPhoneToSMS: 'GET_LIST_PHONE_TO_SMS',
    getListTemplateSMS: 'GET_LIST_TEMPLATE_SMS',
    getListSampleLibraries: 'GET_LIST_SAMPLE_LIBARY',
    getListComment: 'GET_LIST_COMMENT',
    getListAutoJobSelect: 'GET_LIST_AUTOJOB_SELECT',
    getStatisticSaleCampaign: 'GET_STATISTIC_SALE_CAMPAIGN',
    getListAllCampaign: 'GET_LIST_ALL_CAMPAIGN',
    getDetailCampain: 'GET_DETAIL_CAMPAIN',
    getListFBCareCampain: 'GET_LIST_FB_CARE_CAMPAIN',
    getListDataFilterAutomation: 'GET_LIST_DATA_FILTER_AUTOMATION',
    getListDataFieldSaleCampaignAutomation:
      'GET_LIST_DATA_FIELD_SALE_CAMPAIGN_AUTOMATION',
    getListDataFieldFBAutomation: 'GET_LIST_DATA_FIELD_FB_AUTOMATION',
    getListActionAutotaskAutomation: 'GET_LIST_ACTION_AUTO_TASK_AUTOMATION',
    getStatusCampain: 'GET_STATUS_CAMPAIN',
    getListHistoryCampaign: 'GET_LIST_HISTORY_CAMPAIGN',
    getDetailBlockHistoryCampaign: 'GET_DETAIL_BLOCK_HISTORY_CAMPAIGN',
  },
  product: {
    listCollections: 'LIST_COLLECTIONS',
    getListProduct: 'GET_LIST_PRODUCT',
    getListCategory: 'GET_LIST_CATEGORY',
    getListAllReview: 'GET_LIST_ALL_REVIEW',
    getProductSales: 'GET_PRODUCT_SALES',
    getProductHots: 'GET_PRODUCT_HOTS',
    getProductRelated: 'GET_PRODUCT_RELATED',
    getDetailProduct: 'GET_DETAIL_PRODUCT',
    getListAllVoucher: 'GET_LIST_ALL_VOUCHER',
    getListReviewOfProduct: 'GET_LIST_REVIEW_OF_PRODUCT',
    gitListCategory: 'GET_LIST_CATEGORY',
  },

  // home page
  banner: {
    getBanner: 'GET_BANNER',
  },
  category: {
    getCategory: 'GET_CATEGORY',
    getListReviewOfProduct: 'GET_LIST_REVIEW_OF_PRODUCT',
  },
}
